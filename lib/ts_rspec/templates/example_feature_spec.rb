require 'spec_helper' # Always require this in your new file

# Load the dates for winter checkpoint with access to the key (month_name) and value (date)
CONFIG['winter'].each do |month_name, date|
  # Set a feature same as you would in cucumber. js: true means launch a browser window and run the test in the browser.
   feature 'Winter checkpoint', js: true do
    # Add additional cucumber like comments here such as:
    # In order to realize a named business value
    # As an explicit system actor
    # I want to gain some beneficial outcome which furthers the goal
    # This must be comments since Gherikin syntax is not supported at the moment for this implementation
    scenario "Documents created in #{month_name} on #{date} fall into the winter checkpoint range" do
      # => Given a valid user
      login_with_username # login as a default user or pass in a user to login as

      # => And click on documentation for a given class
      goto_documentation_for_class # select a class and click on the documentation tab

      # => And select a child
      select_child_with_value # select a child from the list. you can pass in a child or it defaults to the first child in the list

      # => And add an observation date
      set_observation_date_to date # set a date for the documenation to be logged

      # => And select a note
      find('#bttnToggle_notes1').click
      find('#dimCheckbox_notes1').set(true) # Check a note to be added to the documenation.

      # => And click the save and review method
      save_and_review # Click save and review button

      seconds_to_pause # => Set number of seconds to wait for form to process. Default is 1 second

      # => Then documentation should be in winter checkpoint
      expect(page).to have_content 'Observation Added to Winter 2015/2016'
    end
  end
end

=begin
Note: This is an example test. DO NOT EXPECT IT TO PASS!

Useful documentation:

* https://github.com/jnicklas/capybara

The unabstracted version of the above:

feature 'Group Name of Feature Being Tested', js: true do
  scenario 'Brief summary of what is being tested related to Group Name' do
    login_with_username # abstract method for reusable code
    click_link 'Documentation' # Clicking a link
    click_link 'Add Documentation'
    click_link 'GOLD-7705 Non-Traditional Classroom 1'
    find('span', text: 'Select Children').click # Finding a CSS element to click
    find("input[type='checkbox'][value='6853725']").set(true) # Finding a checkbox based on its value
    click_button 'Close' # clicking a button
    fill_in 'notesDateObserved', with: '09/18/2015' # Adding data to a form element
    find(".ui-datepicker-trigger").click

    find('#bttnToggle_notes1').click #clicking a css element based on its ID
    find('#dimCheckbox_notes1').set(true) # clicking a checkbox based on its ID

    click_button 'Save and Review'
    sleep 1
    expect(page).to have_content 'Observation Added to Winter 2015/2016' # Assertion to check for expected results. There should only be 1 assertion per feature test.
  end
end
=end

