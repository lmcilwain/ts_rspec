=begin
This macro is for methods that abstract function away from the rspec test files.
Create a new macro when it is time to group similar methods together.
=end
def login_with_username(username=CONFIG['user_data']['username'])
  visit 'https://tsiweb4.teachingstrategies.com/gold/teachers/checkpointDates.cfm'
  fill_in 'username', with: username
  puts "Be sure to add a password to spec/support/login.rb"
  fill_in 'password', with: CONFIG['user_data']['password']
  click_button 'Submit'
end
