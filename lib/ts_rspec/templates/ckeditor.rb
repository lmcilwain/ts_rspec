=begin
Used to fill ckeditor fields
# @param [String] locator label text for the textarea or textarea id
Only add methods that relate to ckeditor to this macro.

# Need to look into how to fill in ckeditor.
# CKEDITOR.instances.txtNote selects the object but updateElement('some text'); doesn't appear to be working
# fill_in 'cke_contents_txtNote', with: 'some text'
# fill_in_ckeditor 'cke_txtNote', with: Faker::Lorem.paragraphs(1).first
=end
def fill_in_ckeditor(locator, params = {})
  # Find out ckeditor id at runtime using its label
  locator = find('label', text: locator)[:for] if page.has_css?('label', text: locator)
  # Fill the editor content
  page.execute_script <<-SCRIPT
      var ckeditor = CKEDITOR.instances.#{locator}
      ckeditor.setData('#{params[:with]}')
      ckeditor.focus()
      ckeditor.updateElement()
  SCRIPT
end
