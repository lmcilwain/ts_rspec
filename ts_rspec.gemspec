# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'ts_rspec/version'

Gem::Specification.new do |spec|
  spec.name          = "ts_rspec"
  spec.version       = TsRspec::VERSION
  spec.authors       = ["Vell McIlwain"]
  spec.email         = ["lovell.m@teachingstrategies.com"]

  spec.summary       = %q{Does a basic rspec setup for feature tests}
  spec.description   = %q{Configures an rspec environment to quickly start writing feature tests for websites}
  spec.homepage      = "https://bitbucket.org/lmcilwain/ts_rspec"

  # Prevent pushing this gem to RubyGems.org by setting 'allowed_push_host', or
  # delete this section to allow pushing this gem to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  # spec.bindir        = "exe"
    spec.bindir        = "bin"
  # spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.executables   = ['ts_rspec']
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_dependency 'thor'
  spec.add_dependency 'rspec-core'

  spec.add_development_dependency "bundler", "~> 1.11"
  spec.add_development_dependency "rake", "~> 10.0"
end
