# TsRspec

TsRspec gem creates a minimal rspec environment to quickly get you up and running to create feature tests.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'ts_rspec', '~> 0.4.0', git: 'https://bitbucket.org/lmcilwain/ts_rspec'
```

And then execute:

    $ bundle

## Usage

Run the following command from an empty directory that feature tests will be created from.
## This code overwrites files! Be careful
```
ts_rspec install
```
# Features
* config.yml - Add data to this file when tests are exactly the same but pieces of data change. See example config.yml for details
* selenium webdriver - Firefox browser must be installed on your system
* macros - extract data to macro files in spec/support for creating cleaner tests

# Gems
The following gems are installed when you run the bundle install command
* gem 'rspec' - the testing framework
* capybara - for creating feature specs
* selenium-webdriver - default web driver for running feature specs
* pry - debugger
* pry-nav - debugger
* launchy - screenshotting feature tests
* faker - Data generator

## Contributing

Bug reports and pull requests are welcome on GitHub at https://bitbucket.org/lmcilwain/ts_rspec.
